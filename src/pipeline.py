


def pipeline():
    from google.cloud import bigquery
    from google.oauth2 import service_account

    import pandas as pd


    key_path = 'data/api_key.txt'


    credentials = service_account.Credentials.from_service_account_file(
        key_path,
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    )

    client = bigquery.Client(
        credentials=credentials,
        project=credentials.project_id,
    )


    project_id = 'event-pipeline'


    query = """
    SELECT
      *
    FROM
      angostura.sinluz_rawtweets
    """
    df = pd.read_gbq(query,
                         project_id= 'event-pipeline',
                         dialect='standard')

    return df





